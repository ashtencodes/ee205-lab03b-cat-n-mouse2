#!/bin/bash

DEFAULT_MAX_NUMBER=2048

THE_MAX_VALUE=${1:-$DEFAULT_MAX_NUMBER}

echo The max value is $THE_MAX_VALUE

THE_NUMBER_IM_THINKING_OF=$((RANDOM%$THE_MAX_VALUE+1))

echo OK cat, I’m thinking of a number from 1 to $THE_MAX_VALUE. Make a guess:

aGuess=0

while [ $aGuess -ne $THE_NUMBER_IM_THINKING_OF ]
do
   echo Enter a number:
   read aGuess
   if [[ $aGuess -lt 1 ]]
   then
      echo "You must enter a number that is >= 1"
   elif [[ $aGuess -gt  $THE_MAX_VALUE ]]
   then
      echo "You must enter a number that is <= $THE_MAX_VALUE"
   elif [[ $aGuess -gt $THE_NUMBER_IM_THINKING_OF ]]
   then
      echo "No cat... the number I'm thinking about is smaller than $aGuess"
   elif [[ $aGuess -lt $THE_NUMBER_IM_THINKING_OF ]]
   then
      echo "No cat... the number I'm thinking about is larger than $aGuess"
   fi
done

echo You got me!

echo "      /|_/|
     ( o.o )
      > ^ <"
